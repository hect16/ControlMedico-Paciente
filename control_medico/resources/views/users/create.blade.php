@if(Auth::user()->typeUser != "administrador")
<h2>No tienes permiso para entrar en esta página</h2>
<a href="/home">Volver</a>
@else
@include('security.headerUser')
<div class="panel panel-success">
	<div class="panel-heading">
		<h4>Nueva cita</h4>
	</div>

	<div class="panel-body">
		<form method="post" action="store">
			<p>
				<label>Nombre</label> <br>
				<input type="text" name="name" required autofocus>
			</p>
			<p>
				<label>Apellido</label> <br>
				<input type="text" name="lastName" required autofocus>
			</p>
			<p>
				<label>Sexo</label> <br>
				<input id="sexo" type="radio" name="sexo" value="F" checked>F
				<input id="sexo" type="radio" name="sexo" value="M" >M
			</p>
			<p>
				<label>Fecha de nacimiento</label> <br>
				<input type="date" name="dateBorn" required autofocus>
			</p>
			<p>
				<label>Tipo de usuario</label> <br>
				<SELECT id="typeUser" name="typeUser">
					<option value="medico">MEDICO</option>
					<option value="secretaria">SECRETARIA</option>
					<option value="paciente">PACIENTE</option>
				</SELECT>

			</p>
			<p>
				<label>Correo electrónico</label><br>
				<input type="email" name="email" required>
			</p>
			<p>
				<label>Contraseña</label><br>
				<input id="password" type="password" name="password" required>
			</p>
			<p>
				<label>Confirmar Contraseña</label><br>
				<input id="password" type="password" name="confirmar" required>
			</p>
			<p>
				<input type="submit" value="Guardar" class="btn btn-success">
			</p>
		</form>
	</div>
</div>

@if(Session::has('message'))
<div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>
@endif
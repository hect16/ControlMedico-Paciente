
<!doctype html>
<html ang="{{ app()->getLocale() }}">
<head>
	<title>Diagnósticos</title>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<style>
		body {
			width: 950px;
			margin: 150px auto;
		}
		.badge {
			float: right;
		}	
	</style>
</head>
<body>
	<h1>Diagnósticos</h1>
	<nav class="navbar navbar-default" role="navigation">
  		<div class="container-fluid">
  			<div class="navbar-header">
				<a class="navbar-brand" href="#">R&H projects</a>
  			</div>
    		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      			<ul class="nav navbar-nav">
      				@if(Auth::user()->typeUser == "medico")
        			<li><a href="/diagnostics">Todos</a></li>
        			<li><a href="/diagnostics/create">Nuevo</a></li>
        			<li><a href="/home">Home</a></li>
        			@else
        			<li><a href="/diagnostics/patient">Todos</a></li>
        			<li><a href="/home">Home</a></li>
        			@endif
        		</ul>
        	</div>
        </div>
    </nav>

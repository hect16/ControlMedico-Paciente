@if(Auth::user()->typeUser != "medico")
<h2>No tienes permiso para entrar en esta página</h2>
<a href="/home">Volver</a>
@else
@include('security.headerDiag')
<div class="panel panel-success">
	<div class="panel-heading">
		<h4>Nueva cita</h4>
	</div>

	<div class="panel-body">
		<form method="post" action="store">
			<p>
				<label>Fecha</label> <br>
				<input id="date" type="date" name="date" min="now" required autofocus>
			</p>
			<p>
				<label>Descripción</label> <br>
				<textarea name="remark" required=""></textarea>
			</p>
			<p>
				<label>Médico</label> <br>
				<select name="id_doctor">
					<option value="{{Auth::user()->id}}" selected="">{{Auth::user()->name}} {{Auth::user()->lastName}}</option>
				</select>
			</p>
			<p>
				<label>Paciente</label> <br>
				<select name="id_patient">
					@foreach($users as $user)
					@if($user->typeUser == 'paciente')
					<option value="{{$user->id}}">{{$user->name}} {{$user->lastName}}</option>
					@endif
					@endforeach
				</select>
			</p>
			<p>
				<input type="submit" value="Guardar" class="btn btn-success">
			</p>
		</form>
	</div>
</div>

@if(Session::has('message'))
<div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>
@endif
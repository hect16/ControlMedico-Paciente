@if(Auth::user()->typeUser != "medico")
<h2>No tienes permiso para entrar en esta página</h2>
<a href="/home">Volver</a>
@else
@include('security.headerDiag')
<div class="panel panel-success">
  <div class="panel-heading">
   <h4>Editar Diagnostic</h4>
 </div>

 <div class="panel-body">
  @if (!empty($diagnostic))
  <form method="post" action="/diagnostics/update/{{ $diagnostic->id }}">
    <p>
      <label>Fecha</label>
      <input value="{{ $diagnostic->date }}" type="date" name="date" class="form-control" required>
    </p>
    <p>
      <label>Descripción</label> <br>
      <textarea value="{{ $diagnostic->remark }}" type="text" name="name" class="form-control" required>{{ $diagnostic->remark }}</textarea>
    </p>
    <p>
      <label>Doctor</label>
      <input value="{{ $diagnostic->id_doctor }}" type="numeric" name="id_doctor" class="form-control" required>
    </p>
    <p>
      <label>Paciente</label>
      <input value="{{ $diagnostic->id_patient }}" type="numeric" name="id_patient" class="form-control" required>
    </p>
    <input type="submit" value="Guardar" class="btn btn-success">
    @else
    <p>
      No existe información para éste para el diagnóstico.
    </p>
    @endif
    <a href="/diagnostics" class="btn btn-default">Regresar</a>
  </form>
</div>
</div>

@if(Session::has('message'))
<div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>
@endif
@if(Auth::user()->typeUser != "medico")
<h2>No tienes permiso para entrar en esta página</h2>
<a href="/home">Volver</a>
@else
@include('security.headerDiag')
<div class="panel panel-success">
	<div class="panel-heading">
		<h4>Lista de citas</h4>
	</div>

	<div class="panel-body">
		<table class="table">
			<thead>
				<tr>
					<th>Id</th>
					<th>Fecha</th>
					<th>Descripción</th>
					<th>Médico</th>
					<th>Paciente</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach($diagnostic as $diagnostic)
				@if(Auth::user()->id == $diagnostic->id_doctor)
				<tr>
					<td>{{ $diagnostic->id }}</td>
					<td>{{ $diagnostic->date }}</td>
					<td>{{ $diagnostic->remark }}</td>
					<td>{{ $diagnostic->id_doctor }}</td>
					<td>{{ $diagnostic->id_patient }}</td>
					<td>
						<a href="/diagnostics/show/{{ $diagnostic->id }}"><span class="label label-info">Ver</span></a>
						<a href="/diagnostics/edit/{{ $diagnostic->id }}"><span class="label label-success">Editar</span></a>
						<a href="{{ url('/diagnostics/destroy',$diagnostic->id) }}"><span class="label label-danger">Eliminar</span></a>
					</td>
				</tr>
				@endif
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@if(Session::has('message'))
<div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>
@endif
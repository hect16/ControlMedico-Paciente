@if(Auth::user()->typeUser != "secretaria")
<h2>No tienes permiso para entrar en esta página</h2>
<a href="/home">Volver</a>
@else
@include('security.headerApooi')
<div class="panel panel-success">
	<div class="panel-heading">
		<h4>Nueva cita</h4>
	</div>

	<div class="panel-body">
		<form method="post" action="store">
			<p>
				<label>Fecha de creación</label> <br>
				<input id="dateBorn" type="date" name="created" min="now" required autofocus>
			</p>
			<p>
				<label>Fecha de cita</label> <br>
				<input id="dateBorn" type="date" name="date_appoi" min="now" required autofocus>
			</p>
			<p>
				<label>Médico</label> <br>
				<select name="id_doctor">
					@foreach($users as $user)
					@if($user->typeUser == 'medico')
					<option value="{{$user->id}}">{{$user->name}} {{$user->lastName}}</option>
					@endif
					@endforeach
				</select>
			</p>
			<p>
				<label>Paciente</label> <br>
				<select name="id_patient">
					@foreach($users as $user)
					@if($user->typeUser == 'paciente')
					<option value="{{$user->id}}">{{$user->name}} {{$user->lastName}}</option>
					@endif
					@endforeach
				</select>
			</p>
			<p>
				<input type="submit" value="Guardar" class="btn btn-success">
			</p>
		</form>
	</div>
</div>

@if(Session::has('message'))
<div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>
@endif
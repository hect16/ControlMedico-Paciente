@if(Auth::user()->typeUser != "medico")
<h2>No tienes permiso para entrar en esta página</h2>
<a href="/home">Volver</a>
@else
@include('security.headerMedi')
<div class="panel panel-success">
  <div class="panel-heading">
   <h4>Información del medicamento</h4>
 </div>

 <div class="panel-body">
  @if (!empty($medicine))
  <p>
    Nombre: <strong>{{ $medicine->name}}</strong>
  </p>
  <p>
    Cantidad: <strong>{{ $medicine->quantity }}</strong>
  </p>
  <p>
    Descrición: <strong>{{ $medicine->remark}}</strong>
  </p>
  @else
  <p>
    No existe información para esta cita.
  </p>
  @endif
  <a href="/medicines" class="btn btn-default">Regresar</a>
</div>
</div>
</body>
</html>
@endif
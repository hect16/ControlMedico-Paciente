@if(Auth::user()->typeUser != "medico")
<h2>No tienes permiso para entrar en esta página</h2>
<a href="/home">Volver</a>
@else
@include('security.headerMedi')
	<div class="panel panel-success">
  		<div class="panel-heading">
  			<h4>Editar Medicamento</h4>
  		</div>

  		<div class="panel-body">
        @if (!empty($medicine))
    			<form method="post" action="/medicines/update/{{ $medicine->id }}">
          <p>
            <label>Nombre</label>
            <input value="{{ $medicine->name }}" type="text" name="name" class="form-control" required>
          </p>
          <p>
            <label>Cantidad</label>
            <input value="{{ $medicine->quantity }}" type="numeric" name="quantity" class="form-control" required>
          </p>
          <p>
            <label>Descripción</label>
            <input value="{{ $medicine->remark }}" type="text" name="remark" class="form-control" required>
          </p>
          <input type="submit" value="Guardar" class="btn btn-success">
          @else
          <p>
            No existe información para éste para la cita.
          </p>
          @endif
        <a href="/medicines
        " class="btn btn-default">Regresar</a>
      </form>
		</div>
	</div>

  @if(Session::has('message'))
    <div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
  @endif
</body>
</html>
@endif
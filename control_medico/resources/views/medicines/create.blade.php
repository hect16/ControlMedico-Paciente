@if(Auth::user()->typeUser != "medico")
<h2>No tienes permiso para entrar en esta página</h2>
<a href="/home">Volver</a>
@else
@include('security.headerMedi')
<div class="panel panel-success">
	<div class="panel-heading">
		<h4>Nuevo medicamento</h4>
	</div>

	<div class="panel-body">
		<form method="post" action="store">
			<p>
				<label>Nombre</label> <br>
				<input type="text" name="name" required autofocus>
			</p>
			<p>
				<label>Cantidad</label> <br>
				<input type="numeric" name="quantity" required autofocus>
			</p>
			<p>
				<label>Descripción</label> <br>
				<input type="text" name="remark" required autofocus>
			</p>
			<p>
				<input type="submit" value="Guardar" class="btn btn-success">
			</p>
		</form>
	</div>
</div>

@if(Session::has('message'))
<div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>
@endif
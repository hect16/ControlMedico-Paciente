@if(Auth::user()->typeUser != "medico")
<h2>No tienes permiso para entrar en esta página</h2>
<a href="/home">Volver</a>
@else
@include('security.headerPres')
<div class="panel panel-success">
  <div class="panel-heading">
   <h4>Editar receta</h4>
 </div>

 <div class="panel-body">
  @if (!empty($prescription))
  <form method="post" action="/prescriptions/update/{{ $prescription->id }}">
  <p>
    <label>Detalles</label> <br>
    <textarea value="{{ $prescription->name }}" type="text" name="name" class="form-control" required>{{ $prescription->name }}</textarea>
  </p>
  <p>
    <label>Doctor</label>
    <input value="{{ $prescription->id_doctor }}" type="numeric" name="id_doctor" class="form-control" required>
  </p>
  <p>
    <label>Paciente</label>
    <input value="{{ $prescription->id_patient }}" type="numeric" name="id_patient" class="form-control" required>
  </p>
  <input type="submit" value="Guardar" class="btn btn-success">
  @else
  <p>
    No existe información para éste para la receta.
  </p>
  @endif
  <a href="/prescriptions" class="btn btn-default">Regresar</a>
</form>
</div>
</div>

@if(Session::has('message'))
<div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>
@endif
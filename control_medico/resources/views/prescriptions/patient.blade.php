
@include('security.headerPres')
<div class="panel panel-success">
	<div class="panel-heading">
		<h4>Lista de citas</h4>
	</div>

	<div class="panel-body">
		<table class="table">
			<thead>
				<tr>
					<th>Id</th>
					<th>Detalles</th>
					<th>Médico</th>
					<th>Paciente</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach($prescription as $prescription)
				<tr>
					@if(Auth::user()->id == $prescription->id_patient)
					<td>{{ $prescription->id }}</td>
					<td>{{ $prescription->name }}</td>
					<td>{{ $prescription->id_doctor }}</td>
					<td>{{ $prescription->id_patient }}</td>
					<td>
						<a href="/prescriptions/show/{{ $prescription->id }}"><span class="label label-info">Ver</span></a>
					</td>
					@endif
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@if(Session::has('message'))
<div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>

@include('security.headerPres')

<div class="panel panel-success">
  <div class="panel-heading">
   <h4>Información de la receta</h4>
 </div>

 <div class="panel-body">
  @if (!empty($prescription))
  <p>
    Descripción: <strong>{{ $prescription->name }}</strong>
  </p>
  <p>
    
    Doctor: <strong>{{ $prescription->id_doctor}}</strong>
  </p>
  <p>
    Paciente: <strong>{{ $prescription->id_patient}}</strong>
  </p>
  @else
  <p>
    No existe información para esta receta.
  </p>
  @endif
  <h3>-----------Medicamentos-----------</h3>
  @foreach($medipres as $mepre)
    @if( $mepre->id_prescription ==  $prescription->id )
      @foreach($medicines as $medi)
        @if( $medi->id == $mepre->id_medicine )
          <h4>Nombre: {{$medi->name}}</h4>
          <h4>Descripción: {{$medi->remark}}</h4>
          <h4>Cantidad: {{$medi->quantity}}</h4>
          <h1>---------------------------------------------------------------</h1>
        @endif
      @endforeach
    @endif
  @endforeach
  @if(Auth::user()->id == $prescription->id_doctor)
  <a href="/prescriptions" class="btn btn-default">Regresar</a>
  @else
  <a href="/prescriptions/patient" class="btn btn-default">Regresar</a>
  @endif
</div>
</div>
</body>
</html>
@if(Auth::user()->typeUser != "medico")
<h2>No tienes permiso para entrar en esta página</h2>
<a href="/home">Volver</a>
@else
@include('security.headerMePres')
<div class="panel panel-success">
	<div class="panel-heading">
		<h4>Agregar medicamento</h4>
	</div>

	<div class="panel-body">
		<form method="post" action="store">
			<p>
				<label>Receta</label> <br>
				<input type="text" name="id_prescription" value="{{$prescription->id}}" required autofocus>
			</p>
			<p>
				<label>Medicamento</label> <br>
				<select name="id_medicine">
					@foreach($medicines as $medicine)	
					<option value="{{$medicine->id}}">{{$medicine->name}}</option>
					@endforeach
				</select>
			</p>
			<p>
				<input type="submit" value="Guardar" class="btn btn-success">
			</p>
		</form>
	</div>
</div>

@if(Session::has('message'))
<div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>
@endif
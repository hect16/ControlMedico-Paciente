<?php

namespace App\Http\Controllers;

use App\Diagnostic;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;
use Illuminate\Support\Facades\Redirect;

class DiagnosticController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function patient()
    {
        $diagnostic = Diagnostic::orderBy('id', 'DESC')->get();
        return view('diagnostics.patient')->with('diagnostic',$diagnostic);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $diagnostic = Diagnostic::orderBy('id', 'DESC')->get();
        return view('diagnostics.index')->with('diagnostic',$diagnostic);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$users = User::orderBy('id', 'DESC')->get();
        return view('diagnostics.create')->with("users", $users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $diagnostic = new Diagnostic;
        $diagnostic->date = Input::get('date');
        $diagnostic->remark = Input::get('remark');
        $diagnostic->id_patient = Input::get('id_patient');
        $diagnostic->id_doctor = Input::get('id_doctor');
        if ($diagnostic->save()) {
            Session::flash('message', 'Guardado correctamente');
            Session::flash('class', 'success');
        }
        else{
            Session::flash('message', 'Ha ocurrido un error');
            Session::flash('class', 'danger');
        }
        return Redirect::to('diagnostics/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Medicine  $medicine
     * @return \Illuminate\Http\Response
     */
    public function show($id = null)
    {
        $diagnostic = Diagnostic::find($id);
        return view('diagnostics.show')->with('diagnostic', $diagnostic);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\$Medicine  $medicine
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $diagnostic = Diagnostic::find($id);
        $diagnostic->date = Input::get('date');
        $diagnostic->remark = Input::get('remark');
        $diagnostic->id_patient = Input::get('id_patient');
        $diagnostic->id_doctor = Input::get('id_doctor');

        if ($diagnostic->save()) {
            Session::flash('message', 'Actualizado correctamente');
            Session::flash('class', 'success');
        }
        else{
            Session::flash('message', 'Ha ocurrido un error');
            Session::flash('class', 'danger');
        }
        return Redirect::to('diagnostics/edit/'.$id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Medicine  $medicine
     * @return \Illuminate\Http\Response
     */
    public function edit($id = null)
    {
        $diagnostic = Diagnostic::find($id);
        return view('diagnostics.edit')->with('diagnostics', $diagnostic);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Medicine  $medicine
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $diagnostic = Diagnostic::find($id);
        if ($diagnostic->delete()) {
            Session::flash('message', 'Eliminado correctamente');
            Session::flash('class', 'success');
        }
        else{
            Session::flash('message', 'Ha ocurrido un error');
            Session::flash('class', 'danger');
        }
        return Redirect::to('diagnostics');

    } 
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function generatePDF()

    {

        $data = ['title' => 'Welcome to HDTuto.com'];

        $pdf = PDF::loadView('myPDF', $data);



        return $pdf->download('hdtuto.pdf');

    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diagnostic extends Model
{
    protected $fillable=['id','date', 'remark', 'id_patient', 'id_doctor',];
}
